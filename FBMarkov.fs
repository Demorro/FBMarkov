open FSharp.Data
open System
open System.Collections.Generic;


 // Wee little script to take the archived chat information Facebook provides you, and generate Markov chains for a person based on that.
 // You'll probably get much better results using yourself, as you'll have much more data for yourself compared to what a single person has said to you,
 // but you can choose who to use as you like.
 // 
 // This isn't a fully done "thing", so you'll need to modify a couple of strings below to get this working.
 // First, download your facebook archive data from facebook. Last time i checked, it's in your general account settings, near the bottom : "Download a copy of your Facebook data"
 // You'll get a big ol' archive. Unzip it and navigate the the messages folder. You should see a bunch of files names 0,1,2,3.html..
 // You need to combine these all into one html file. I do this by running "for %f in (*.html) do type "%f" >> messages.html"
 // Great, this is your input file, put this somewhere, and change the "let chatDocument = " path below to point to it.
 //
 // Next, you need to set the name of the person you're looking to generate markov messages for. Make sure you get it exact
 // Navigate below to the "friendToMarkov =" line and set it.
 //
 // Now you should be ready to go, compile yo' F# script and run.
 // Now, I've never tested this with any dataset beside my own, so I wouldn't be surprised it this just flat out dosen't work, ach well, good luck.
 //
 // Disclaimer : This was a learning F# project, and is terrible, use at your own risk.


//Generate the map of what words occur after what phrases for a specific person in a messages.html file
let WordOccuranceMap friendToMarkov = 

    //The html document from facebook archives, all stitched together. 
    let chatDocument = HtmlDocument.Load("../FBChats/messages.htm");

    //Remove all the html tag's I'm not interested in, get em out of here.
    let chatMessages = chatDocument.Descendants("p",true) |> Seq.map(fun x -> x.ToString().Replace("<p>","")) |> Seq.map(fun x -> x.Replace("</p>",""))
    let chatDataTags = chatDocument.Descendants("div", true) |> Seq.collect(fun x->x.Descendants("span",false)) |> Seq.filter(fun y->y.HasClass("user")) |> Seq.map(fun z -> z.InnerText())   

    //Extract the sentances said by the person of interest
    let linkedChatAndName = Seq.zip chatDataTags chatMessages |> Seq.filter(fun x -> fst x = friendToMarkov) |> Seq.rev
    let chatSentances = linkedChatAndName |> Seq.map(fun x -> snd x) |> Seq.map(fun x -> x.Split[|' '|])
    
    let MARKOV_MAG = 2; //How long the keys of the chain should be, the lower, the less stable, but more interesting

    //Build a dictionary of word occurance mapped to phrases that came before. This is the core of how the markov chain works.
    //This is a poorly written segment, it's not very understandable. Remember to hate yourself for the rest of your life.
    let chatSentanceWordWindows = chatSentances |> Seq.map(fun x -> Array.windowed (MARKOV_MAG + 1) x)
    let wordKeyValuePairs = chatSentanceWordWindows |> Seq.map(fun x -> x |> Array.map(fun y -> (((Array.get y 0).ToString().ToLower(), (Array.get y 1).ToString().ToLower()), (Array.get y 2).ToString().ToLower())))
    
    let wordOccuranceDict = new Dictionary<(String * String), List<String>>();
    let LoadDoubleMagKeyValuePairIntoDict (dict : Dictionary<(String * String), List<String>>) keyValPair =        
        if(dict.ContainsKey(fst keyValPair)) then
            dict.[fst keyValPair].Add(snd keyValPair)
            ()
        else
            dict.Add(fst keyValPair, new List<String>())
            dict.[fst keyValPair].Add(snd keyValPair)
            ()  

    //Load the key value pair of phrase -> next word into the dictionary so we have a full set of phrase->words that occur next. 
    wordKeyValuePairs |> Seq.iter(fun x -> x |> Array.iter(fun y -> y |> LoadDoubleMagKeyValuePairIntoDict wordOccuranceDict))
    wordOccuranceDict, chatSentances

//Taking the dictionary of word-frequency from prior phrases, generate a markov chain.
let GenerateChain (wordMapDict : Dictionary<(string * string), List<string>>) sentances = 
    let rnd = System.Random();

    //Generate the chain recursively wia getting one of the candidate next words randomly from the dictionary entry concerning that  phrase
    let rec chain (key : (String * String)) = [

        //Its possible to not find a word from a phrase, which means this is where a sentance has ended, and where this chain ends, see match false below
        let (nextWordFound,nextWrdList) = wordMapDict.TryGetValue(key)
        
        match nextWordFound with
        | true ->  
            let nextWord = nextWrdList.[rnd.Next(0, wordMapDict.[key].Count)]
            yield nextWord
            yield! chain (snd key, nextWord)
        | false -> ignore
    ]

    //Pick a seed to start the markov from randomly from the available start phrases.
    let startingStrings = sentances |> Seq.filter(fun x -> x |> Array.length >= 2) |> Seq.map(fun y -> [y.[0]; y.[1]]) |> Seq.filter(fun q -> wordMapDict.ContainsKey((q.Item 0, q.Item 1))) |> Seq.filter(fun z -> wordMapDict.[(z.Item 0, z.Item 1)].Count >= 2)   
    let startStringPair = startingStrings |> Seq.toList |> List.item (rnd.Next(0, startingStrings |> Seq.length))

    //Generate the markov chain from the selected starting string
    let generatedChain = chain (startStringPair.Item 0, startStringPair.Item 1)
    //chain call dosen't include the starting strings, so apprend them.
    let finishedMarkovChain = generatedChain |> List.append [startStringPair.Item 0; startStringPair.Item 1]

    finishedMarkovChain

[<EntryPoint>]
let main argv = 
    //Put your own name here, exactly.
    let friendToMarkov = "Elliot Charles Morris"
    let wordMapAndSentances = WordOccuranceMap friendToMarkov
   
    Console.WriteLine("");

    //Create 16 markov chains and ouput them.
    [0..16] |> List.iter(fun y -> 
        let chain = GenerateChain (fst wordMapAndSentances) (snd wordMapAndSentances)
        chain |> List.iter(fun x -> Console.Write(x + " "))
        Console.WriteLine("");
        Console.WriteLine("");
    )

    Console.ReadLine()
    0 // return an integer exit code
