**FBMarkov. Generate Markov chains from your Facebook chat data**

Wee little script to take the archived chat information Facebook provides you, and generate random Markov chains that look similar to your speech patterns.

Sample of generated output from my facebook data :

![Generated from FB data going all the way back to the beginning when I was like, 13](https://i.imgur.com/LXUPzZB.png)
 
This isn't a fully done "thing", so you'll need to modify a couple of strings below to get this working.
First, download your facebook archive data from facebook. Last time i checked, it's in your general account settings, near the bottom : "Download a copy of your Facebook data"
You'll get a big ol' archive. Unzip it and navigate the the messages folder. You should see a bunch of files names 0,1,2,3.html..
You need to combine these all into one html file. I do this by running "for %f in (*.html) do type "%f" >> messages.html"
Great, this is your input file, put this somewhere, and change the "let chatDocument = " path below to point to it.

Next, you need to set your name. Make sure you get it exact
Navigate below to the "friendToMarkov =" line and set it.
Only the name of the person who generated the input file is supported, can't guarentee it'll work for others (it probably wont, and you likely don't have a big enough input set anyway. Maybe that's just me though, for obvious reasons my friends barely speak to me)

Now you should be ready to go, compile yo' F# script and run.
Now, I've never tested this with any dataset beside my own, so I wouldn't be surprised it this just flat out dosen't work, ach well, good luck.

Disclaimer : This was a learning F# project, and is terrible, use at your own risk.
